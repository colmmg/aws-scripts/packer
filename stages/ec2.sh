AWS_IAM_PROFILE_NAME="packer"
ENA_SUPPORT="true"

setAmiBuildRegion() {
  AMI_BUILD_REGION="$AWS_REGION"
}

getEc2BuildVariables() {
  getDeviceName
  getInstanceType
  getVolumeSize
  getSshUserName
  getSourceAmiId
  getSecurityGroupId
  getKmsKeyMap
}

getDeviceName() {
  DEVICE_NAME=`grep -P "(\s+|^)device_name:" chef/$CHEF_REPO/kitchen.yml |cut -d":" -f2 |xargs`
  if [ ! "$DEVICE_NAME" ]; then
    printf "No device name found in kitchen.yml!\n"
    exit 1
  fi
}

getInstanceType() {
  INSTANCE_TYPE="`grep -P -m 1 "(\s+|^)instance_type:" chef/$CHEF_REPO/kitchen.yml |cut -d':' -f2 |xargs`"
  if [ ! "$INSTANCE_TYPE" ]; then
    printf "No instance type found in kitchen.yml!\n"
    exit 1
  fi
}

getVolumeSize() {
  VOLUME_SIZE=`grep -P "(\s+|^)volume_size:" chef/$CHEF_REPO/kitchen.yml |cut -d":" -f2 |xargs`
  if [ ! "$VOLUME_SIZE" ]; then
    printf "No volume size found in kitchen.yml!\n"
    exit 1
  fi
}

getSshUserName() {
  SSH_USER_NAME=`grep -P "(\s+|^)username:" chef/$CHEF_REPO/kitchen.yml |cut -d":" -f2 |xargs`
  if [ ! "$SSH_USER_NAME" ]; then
    printf "No ssh user name found in kitchen.yml!\n"
    exit 1
  fi
}

getSourceAmiId() {
  if grep -Pq "(\s+|^)image_id:" "chef/$CHEF_REPO/kitchen.yml"; then
    SOURCE_AMI_ID="`grep -P -m 1 "(\s+|^)image_id:" chef/$CHEF_REPO/kitchen.yml |cut -d':' -f2 |xargs`"
  else
    local source_ami_name_tag="$(grep -P '(\s+|^)tag:Name:' chef/$CHEF_REPO/kitchen.yml |cut -d"\"" -f2 |xargs)"
    local source_ami_branch_tag="$(grep -P '(\s+|^)tag:GitBranch:' chef/$CHEF_REPO/kitchen.yml |cut -d"\"" -f2 |xargs)"
    SOURCE_AMI_ID=$(aws ec2 describe-images \
                    --filters "Name=owner-id,Values=$AWS_ACCOUNT_ID" \
                              "Name=tag:Name,Values=$source_ami_name_tag" \
                              "Name=tag:GitBranch,Values=$source_ami_branch_tag" \
                    --query 'reverse(sort_by(Images, &CreationDate))[].ImageId' \
                    --output=text \
                    --region=$AMI_BUILD_REGION \
                    |awk '{print $1}')
  fi
  if [ ! "$SOURCE_AMI_ID" ]; then
    printf "Unable to find source AMI!\n"
    exit 1
  fi
}

getSecurityGroupId() {
  local security_group_name="$(curl -s http://169.254.169.254/latest/meta-data/security-groups)"
  if [ ! "$security_group_name" ]; then
    printf "Could not find security group name from instance meta-data!\n"
    exit 1
  fi
  SECURITY_GROUP_ID="$(aws ec2 describe-security-groups \
                       --region $AMI_BUILD_REGION \
                       --filters "Name=vpc-id,Values=$AWS_VPC_ID" "Name=group-name,Values=$security_group_name" \
                       --query 'SecurityGroups[0].GroupId' \
                       --output=text)"
  if [ ! "$SECURITY_GROUP_ID" ]; then
    printf "Could not find the security group id for security group name $security_group_name!\n"
    exit 1
  fi
}

getKmsKeyMap() {
  BUILD_REGION_KMS_KEY_ID=`aws kms describe-key --key-id alias/ami-kms --query 'KeyMetadata.KeyId' --output text --region $AMI_BUILD_REGION`
  REGION_KMS_KEY_IDS+="\"$AMI_BUILD_REGION\":\"${BUILD_REGION_KMS_KEY_ID}\""
}

checkIfAmiExists() {
  local ami_name=$(aws ec2 describe-images \
                   --filters "Name=owner-id,Values=$AWS_ACCOUNT_ID" \
                             "Name=name,Values=$AMI_NAME" \
                   --query 'Images[*].Name' \
                   --output=text \
                   --region=$AMI_BUILD_REGION 2> /dev/null)
  if [ "$ami_name" == "$AMI_NAME" ]; then
    AMI_EXISTS="true"
  fi
}

updateEc2PackerTemplate() {
  sed -i -e "s,__AMI_BUILD_REGION__,$AMI_BUILD_REGION,g" ec2.json
  sed -i -e "s/__AMI_DEPLOY_REGIONS__/$AMI_DEPLOY_REGIONS_STRING/g" ec2.json
  sed -i -e "s,__AMI_NAME__,$AMI_NAME,g" ec2.json
  sed -i -e "s,__AWS_ACCOUNT_ID__,$AWS_ACCOUNT_ID,g" ec2.json
  sed -i -e "s,__AWS_IAM_PROFILE_NAME__,$AWS_IAM_PROFILE_NAME,g" ec2.json
  sed -i -e "s,__AWS_SUBNET_ID__,$AWS_SUBNET_ID,g" ec2.json
  sed -i -e "s,__AWS_VPC_ID__,$AWS_VPC_ID,g" ec2.json
  sed -i -e "s|__BUILD_REGION_KMS_KEY_ID__|$BUILD_REGION_KMS_KEY_ID|g" ec2.json
  sed -i -e "s,__BUILD_USER__,$(whoami),g" ec2.json
  sed -i -e "s,__COOKBOOK_NAME__,$COOKBOOK_NAME,g" ec2.json
  sed -i -e "s,__DEVICE_NAME__,$DEVICE_NAME,g" ec2.json
  sed -i -e "s,__ENA_SUPPORT__,$ENA_SUPPORT,g" ec2.json
  sed -i -e "s,__GIT_BRANCH__,$GIT_BRANCH,g" ec2.json
  sed -i -e "s,__GIT_SHORT_SHA__,$GIT_SHORT_SHA,g" ec2.json
  sed -i -e "s,__INSTANCE_TYPE__,$INSTANCE_TYPE,g" ec2.json
  sed -i -e "s|__REGION_KMS_KEY_IDS__|$REGION_KMS_KEY_IDS|g" ec2.json
  sed -i -e "s,__SECURITY_GROUP_ID__,$SECURITY_GROUP_ID,g" ec2.json
  sed -i -e "s,__SOURCE_AMI_ID__,$SOURCE_AMI_ID,g" ec2.json
  sed -i -e "s,__SSH_USER_NAME__,$SSH_USER_NAME,g" ec2.json
  sed -i -e "s,__VOLUME_SIZE__,$VOLUME_SIZE,g" ec2.json
}

getBuildRegionDeployedAmi() {
  BUILD_REGION_DEPLOYED_AMI=$(aws ec2 describe-images \
                              --filters "Name=owner-id,Values=$AWS_ACCOUNT_ID" \
                                        "Name=name,Values=$AMI_NAME" \
                              --query 'Images[*].ImageId' \
                              --output=text \
                              --region=$AMI_BUILD_REGION 2> /dev/null)
  if [ ! "$BUILD_REGION_DEPLOYED_AMI" ]; then
    printf "Could not find built ami for build region!\n"
    exit 1
  fi
}

copyAmiToDeployRegions() {
  local region
  local ami_id
  declare -gA DEPLOYED_AMIS
  DEPLOYED_AMIS[$AMI_BUILD_REGION]="$BUILD_REGION_DEPLOYED_AMI"
  for region in ${AMI_DEPLOY_REGIONS[@]}; do
    if [ "$region" == "$AMI_BUILD_REGION" ]; then
      continue
    fi
    ami_id=$(aws ec2 describe-images \
             --filters "Name=owner-id,Values=$AWS_ACCOUNT_ID" \
                       "Name=name,Values=$AMI_NAME" \
             --query 'Images[*].ImageId' \
             --output=text \
             --region=$region 2> /dev/null)
    if [ ! "$ami_id" ]; then
      ami_id=$(aws ec2 copy-image \
               --encrypted \
               --name $AMI_NAME \
               --source-image-id $BUILD_REGION_DEPLOYED_AMI \
               --source-region $AMI_BUILD_REGION \
               --region $region \
               --kms-key-id alias/ami-kms \
               --output text 2> /dev/null)
    fi
    DEPLOYED_AMIS[$region]="$ami_id"
  done
  if [ ! ${#DEPLOYED_AMIS[@]} -eq ${#AMI_DEPLOY_REGIONS[@]} ]; then
    printf "Could not find built amis for all deploy regions!\n"
    exit 1
  fi
}

writeDeployedAmisToFile() {
  local region
  local ami_id
  echo "$AWS_ENVIRONMENT" >> amis.txt
  for region in "${!DEPLOYED_AMIS[@]}"; do
    ami_id=${DEPLOYED_AMIS[$region]}
    echo " - ${region}: ${ami_id}" >> amis.txt
  done
  echo "" >> amis.txt
}

checkIfChefRepoHasKitchenFile
getBuildVariables
setAmiBuildRegion
getEc2BuildVariables
checkIfAmiExists
if [ "$AMI_EXISTS" != "true" ]; then
  updateEc2PackerTemplate
  packerBuild
fi
getBuildRegionDeployedAmi
copyAmiToDeployRegions
writeDeployedAmisToFile
