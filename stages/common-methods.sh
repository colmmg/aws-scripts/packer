checkIfChefRepoHasKitchenFile() {
  if [ ! -e chef/$CHEF_REPO/kitchen.yml ]; then
    printf "No kitchen.yml file found in chef/$CHEF_REPO!\n"
    exit 1
  fi
}

getBuildVariables() {
  getCookbookName
  getGitShortSha
  getAmiName
}

getCookbookName() {
  COOKBOOK_NAME="`grep -P -m 1 "(\s+|^)name" chef/$CHEF_REPO/metadata.rb |cut -d"'" -f2`"
  if [ ! "$COOKBOOK_NAME" ]; then
    printf "No cookbook name found in metadata.rb!\n"
    exit 1
  fi
}

getGitShortSha() {
  pushd chef/$CHEF_REPO
  GIT_SHORT_SHA="$(git rev-parse --short=8 HEAD)"
  if [ ! "$GIT_SHORT_SHA" ]; then
    printf "Unable to detect Git Short SHA!\n"
  fi
  popd
}

getAmiName() {
  AMI_NAME="$COOKBOOK_NAME-$GIT_BRANCH-$GIT_SHORT_SHA"
}

packerBuild() {
  ./packer build ${PIPELINE_STAGE}.json
  if [ "$?" != 0 ]; then
    exit 1
  fi
}
