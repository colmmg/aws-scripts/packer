exportGitSSHCommand() {
  if [ ! "$GIT_SSH_KEY" ]; then
    printf "No GIT_SSH_KEY variable set!\n"
    exit 1
  fi
  export GIT_SSH_COMMAND="ssh -i $GIT_SSH_KEY"
}

cloneChefRepo() {
  git clone git@${GIT_HOST}:${GIT_GROUP}/$CHEF_REPO.git chef/$CHEF_REPO
  if [ "$?" != 0 ]; then
    printf "Error cloning chef/${CHEF_REPO}!\n"
    exit 1
  fi
  pushd chef/$CHEF_REPO
  git checkout $GIT_BRANCH
  if [ "$?" != 0 ]; then
    printf "Error checking out $GIT_BRANCH on chef/${CHEF_REPO}!\n"
    exit 1
  fi
  popd
}

downloadPacker() {
  curl "https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip" -o packer.zip
  unzip packer.zip
  if [ ! -e packer ]; then
    printf "Failed to download packer version ${PACKER_VERSION}!\n"
    exit 1
  fi
}

berksPackage() {
  pushd chef/$CHEF_REPO
  berks package ../../cookbooks.tar.gz
  if [ ! -e ../../cookbooks.tar.gz ]; then
    printf "Error running berks package!\n"
    exit 1
  fi
  popd
}

exportGitSSHCommand
cloneChefRepo
downloadPacker
berksPackage
