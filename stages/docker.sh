getDockerBuildVariables() {
  getSourceDockerImage
}

getSourceDockerImage() {
  SOURCE_DOCKER_IMAGE="`grep -P -m 1 "(\s+|^)image:" chef/$CHEF_REPO/kitchen.yml |cut -d':' -f2- |xargs`"
  if [ ! "$SOURCE_DOCKER_IMAGE" ]; then
    printf "No source docker image found in kitchen.yml!\n"
    exit 0
  fi
  SOURCE_DOCKER_IMAGE="$(echo "$SOURCE_DOCKER_IMAGE" | sed "s/<%= ENV\[REGISTRY_HOST\] %>/$REGISTRY_HOST/g")"
}

updateDockerPackerTemplate() {
  sed -i -e "s,__SOURCE_DOCKER_IMAGE__,$SOURCE_DOCKER_IMAGE,g" docker.json
  sed -i -e "s,__COOKBOOK_NAME__,$COOKBOOK_NAME,g" docker.json
  sed -i -e "s,__AWS_REGION__,$AWS_REGION,g" docker.json
  sed -i -e "s,__AWS_ACCOUNT_ID__,$AWS_ACCOUNT_ID,g" docker.json
}

checkIfChefRepoHasKitchenFile
getBuildVariables
getDockerBuildVariables
updateDockerPackerTemplate
packerBuild
