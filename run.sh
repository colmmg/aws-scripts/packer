#!/bin/bash

export PATH=$PATH:/usr/local/bin
GIT_HOST="gitlab.com"
GIT_GROUP="colmmg"

while getopts "s:" opt; do
  case "$opt" in
    s ) PIPELINE_STAGE="$OPTARG" ;;
  esac
done

sourceEc2Info() {
  if [ ! -e /etc/ec2-info.sh ]; then
    printf "No /etc/ec2-info.sh found!\n"
    exit 1
  fi
  source /etc/ec2-info.sh
}

getJenkinsBuildParameters() {
  GIT_BRANCH="$Branch"
  PACKER_VERSION="$PackerVersion"
  AMI_DEPLOY_REGIONS_STRING="${DeployRegions}"
  if [ ! "$GIT_BRANCH" ]; then
    GIT_BRANCH=$(grep "Branch" Jenkinsfile |grep defaultValue |cut -d "'" -f2)
  fi
  if [ ! "$PACKER_VERSION" ]; then
    PACKER_VERSION=$(grep "PackerVersion" Jenkinsfile |cut -d "'" -f2)
  fi
  if [ ! "$AMI_DEPLOY_REGIONS_STRING" ]; then
    AMI_DEPLOY_REGIONS_STRING=$(grep "DeployRegions" Jenkinsfile |cut -d "'" -f2)
  fi
  AMI_DEPLOY_REGIONS=( ${AMI_DEPLOY_REGIONS_STRING//[,]/ } )
}

getChefRepoFromJenkinsJobName() {
  if [ ! "$JOB_NAME" ]; then
    printf "Could not detect Jenkins job name!\n"
    exit 1
  fi
  CHEF_REPO="$(echo $JOB_NAME | sed 's,-packer,,g')"
  if [ ! "$CHEF_REPO" ]; then
    printf "Error getting Chef repo from Jenkins job name!\n"
    exit 1
  fi
}

runPipelineStage() {
  source stages/common-methods.sh
  source stages/${PIPELINE_STAGE}.sh
}

sourceEc2Info
getJenkinsBuildParameters
getChefRepoFromJenkinsJobName
runPipelineStage
