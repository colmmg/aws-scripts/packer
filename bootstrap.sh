#!/bin/bash

CHEF_VERSION="15.3.14"
INSPEC_VERSION="4.18.0"
export PATH=$PATH:/usr/local/bin

while getopts "c:d" opt; do
  case "$opt" in
    c ) COOKBOOK_NAME="$OPTARG" ;;
    d ) DOCKER="true" ;;
  esac
done

sourceEc2Info() {
  if [ -e /etc/ec2-info.sh ]; then
    source /etc/ec2-info.sh
  fi
}

validateInputs() {
  if [ ! "$COOKBOOK_NAME" ]; then
    printf "No cookbook name provided! Usage: bash /etc/chef/bootstrap/bootstrap.sh -c COOKBOOK_NAME\n"
    handleError
  fi
}

installRequiredPackages() {
  yum install -y sudo hostname tar
}

installChef() {
  if [[ "`rpm -q chef`" != *"$CHEF_VERSION"* ]]; then
    if [[ "`rpm -q chef`" != *"is not installed"* ]]; then
      rpm -e chef
    fi
    local chef_download_path="$CHEF_VERSION/el/7/chef-$CHEF_VERSION-1.el7.x86_64.rpm"
    curl "https://packages.chef.io/files/stable/chef/${chef_download_path}" -o /tmp/chef.rpm
    rpm -ivh /tmp/chef.rpm
    rm -f /tmp/chef.rpm
  fi
}

installInspec() {
  if [[ "`rpm -q inspec`" != *"$INSPEC_VERSION"* ]]; then
    if [[ "`rpm -q inspec`" != *"is not installed"* ]]; then
      rpm -e inspec
    fi
    local inspec_download_path="$INSPEC_VERSION/el/7/inspec-$INSPEC_VERSION-1.el7.x86_64.rpm"
    curl "https://packages.chef.io/files/stable/inspec/${inspec_download_path}" -o /tmp/inspec.rpm
    rpm -ivh /tmp/inspec.rpm
    rm -f /tmp/inspec.rpm
  fi
}

unpackCookbooks() {
  if [ ! -e /etc/chef/bootstrap/cookbooks.tar.gz ]; then
    printf "No cookbooks at /etc/chef/bootstrap/cookbooks.tar.gz!\n"
    exit 1
  fi
  pushd /etc/chef/bootstrap
  tar zxf cookbooks.tar.gz
  cp -r cookbooks/${COOKBOOK_NAME}/environments .
  popd
}

runChefClient() {
  local docker_chef_attribute="false"
  if [ "$DOCKER" == "true" ]; then
    docker_chef_attribute="true"
  fi
  echo "{ \"colmmg\": { \"docker\": $docker_chef_attribute } }" > /etc/chef/bootstrap/attributes.json
  pushd /etc/chef/bootstrap
  /usr/bin/chef-client --chef-license accept -z -E build -o ${COOKBOOK_NAME}::default -j /etc/chef/bootstrap/attributes.json
  popd
}

runInspec() {
  pushd /etc/chef/bootstrap
  if [ "`ls cookbooks/$COOKBOOK_NAME/test/default/*.rb 2>/dev/null`" ]; then
    inspec exec cookbooks/$COOKBOOK_NAME/test/default/ || exit 1
  fi
  if [ "$DOCKER" == "true" ]; then
    if [ "`ls cookbooks/$COOKBOOK_NAME/test/docker/*.rb 2>/dev/null`" ]; then
      inspec exec cookbooks/$COOKBOOK_NAME/test/docker/ || exit 1
    fi
  else
    if [ "`ls cookbooks/$COOKBOOK_NAME/test/ec2/*.rb 2>/dev/null`" ]; then
      inspec exec cookbooks/$COOKBOOK_NAME/test/ec2/ || exit 1
    fi
  fi
  popd
}

cleanup() {
  rm -rf /etc/chef/bootstrap
  yum clean all
  rm -rf /var/cache/yum
  rm -rf /root/.chef/local-mode-cache/cache/cookbooks/
  if [ "$DOCKER" == "true" ]; then
    rpm -e chef inspec
  fi
}

sourceEc2Info
validateInputs
installRequiredPackages
installChef
installInspec
unpackCookbooks
runChefClient
runInspec
cleanup
